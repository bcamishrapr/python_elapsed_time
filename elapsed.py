from datetime import datetime
import sys
#date_1 = '2021-12-01_05:54:57'
#date_2 = '2021-12-01_06:00:45'
date_1 = sys.argv[1]
date_2 = sys.argv[2]


#date_format_str = '%d/%m/%Y %H:%M:%S.%f'
date_format_str = '%Y-%m-%d_%H:%M:%S'

start = datetime.strptime(date_1, date_format_str)
end =   datetime.strptime(date_2, date_format_str)
#end =   datetime.strptime(date_2, date_format_str)

# Get interval between two timstamps as timedelta object
diff = end - start
# Get interval between two timstamps in hours
diff_in_hours = diff.total_seconds() / 60
print('Difference between two datetimes in hours:')
print(diff_in_hours)
